for f in *; do
    if [ -d "$f" ]; then
    
        if [[ "$f" == *"$class"* ]]; then
          echo "--------------------------------------"
          echo "STASH AND PULL: $f"
          cd $f
          git stash
          git config pull.rebase false
          git pull
          git checkout main
          git pull
          cd ..
        fi
    fi
done
