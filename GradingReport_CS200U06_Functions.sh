unit_folder="u06_Functions"
path="/media/rachelwil/RachelsSSD1/TEACHING/STUDENT-REPOS/2024-01/CS200/$unit_folder.html"
rm $path
touch $path

echo "<head>" >> $path
echo "<title> $unit_folder </title>" >> $path
echo "<style type='text/css'>" >> $path
echo "pre { border: solid 1px #000; padding: 10px; }" >> $path
echo "pre.output { background: #111; color: #eee; }" >> $path
echo "tr { vertical-align:top; }" >> $path
echo "td, th { text-align: left; width: 50%; overflow: scroll; }" >> $path
echo "nav { float: left; width: 15%; position: fixed; display: block;  }" >> $path
echo "nav ul li {  }" >> $path
echo "section { display: block; float: left; margin-left: 15%; width: 80%; } " >> $path
echo "</style>" >> $path

echo "<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/default.min.css'>" >> $path
echo "<script src='https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js'></script>" >> $path
echo "<script>hljs.initHighlightingOnLoad();</script>" >> $path

echo "</head><body>" >> $path

echo "<nav>" >> $path
echo "<h1> $unit_folder </h1>" >> $path

echo "<ul>" >> $path
for f in *; do
    if [ -d "$f" ]; then
    
        if [[ "$f" == *"$class"* ]]; then
            echo "<li> <a href='#$f'>$f</a> </li>" >> $path
        fi
    fi
done 
echo "</ul>" >> $path
echo "</nav>" >> $path

echo "<section>" >> $path        
        
for f in *; do
    if [ -d "$f" ]; then
    
        if [[ "$f" == *"$class"* ]]; then
          echo "--------------------------------------"
          echo $f
          
          echo "<hr>" >> $path
          echo "<h2> <a href='#$f' name='$f'> $f </a> </h2>" >> $path
          
          echo "<table><tr><th style='width:50%;'> <h3>graded programs</h3> </th><th style='width:50%;'> <h3>practice programs</h3> </th></tr>" >> $path
          
          cd $f/$unit_folder/graded_program
          
          echo "<tr>" >> $path
          echo "<td>" >> $path
          echo "<h3>graded_program</h3>" >> $path
          
          if g++ *.h *.cpp -o $unit_folder; then
              echo "<p>Program output:</p>" >> $path
              echo "<pre class='output'>" >> $path
              echo "1 2.25 1 4.00 1 5.30 2 1 1.10 1 3.00 2 2 2" | ./$unit_folder test >> $path          
              echo "</pre>" >> $path
          else
            echo "<p> BUILD FAILED: </p>" >> $path
            echo "<pre>" >> $path
            g++ *.h *.cpp -o $unit_folder >> $path 2>> $path # 2nd one is error log
            echo "</pre>" >> $path
          fi
          
          
          echo "graded_program/Functions.cpp" >> $path
          
          echo "<p><a href='$f/$unit_folder/graded_program/Functions.cpp'> open code </a>" >> $path
          
          echo "<pre><code class='cpp'>" >> $path
          cat Functions.cpp >> $path
          echo "</code></pre>" >> $path
          
          echo "</td><td>" >> $path
          
          
          
          cd ../practice1_NoInNoOut          
          echo "practice1_NoInNoOut/program1.cpp" >> $path
          echo "<pre><code class='cpp'>" >> $path
          cat program1.cpp >> $path
          echo "</code></pre>" >> $path
          
          cd ../practice2_YesInNoOut          
          echo "practice2_YesInNoOut/program2.cpp" >> $path
          echo "<pre><code class='cpp'>" >> $path
          cat program2.cpp >> $path
          echo "</code></pre>" >> $path
          
          cd ../practice3_NoInYesOut          
          echo "practice3_NoInYesOut/program3.cpp" >> $path
          echo "<pre><code class='cpp'>" >> $path
          cat program3.cpp >> $path
          echo "</code></pre>" >> $path
          
          cd ../practice4_YesInYesOut          
          echo "practice4_YesInYesOut/program4.cpp" >> $path
          echo "<pre><code class='cpp'>" >> $path
          cat program4.cpp >> $path
          echo "</code></pre>" >> $path
          
          
          
          
          
          
          echo "</td>" >> $path
          echo "</tr></table>" >> $path
          
          cd ../../..
          pwd
        fi
    fi
done

echo "</section>" >> $path

echo "</body>" >> $path
