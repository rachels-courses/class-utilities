exename="CPPReviewClasses_out"
origpath=$(pwd)

for f in *; do
    if [ -d "$f" ]; then
    
        if [[ "$f" == *"$class"* ]]; then
          echo "--------------------------------------"
          # Find all makefiles
          find $f -name "$exename" -print0 |
            while IFS= read -r -d '' line; do
              echo "PATH $line"
              EXE="./$line"
              ARG="1"
              RUNNER="echo $ARG | $EXE"
              echo "RUNNER: $RUNNER" 
              RESULT=$(eval "$RUNNER")
            done
            echo "HIT ENTER"
            read
        fi
    fi
done
