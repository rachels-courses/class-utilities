paths=( "/home/rachelwil/TEACHING/courses/202401_Spring/Course3_ObjectOrientedProgramming/labs/starter_code/u18_OperatorOverloading" "/home/rachelwil/TEACHING/courses/202401_Spring/Course3_ObjectOrientedProgramming/labs/starter_code/u19_ThirdPartyLibraries" ) # "/home/rachelwil/TEACHING/courses/202401_Spring/Course3_ObjectOrientedProgramming/labs/starter_code/cs235_application_version_u12u13" )

for f in *; do
    if [ -d "$f" ]; then
    
        if [[ "$f" == *"$class"* ]]; then
          echo "--------------------------------------"
          echo "DIR: $f"
          for path in "${paths[@]}"
          do
            echo ">> cp -r $path $f"
            cp -r $path $f
          done
        fi
    fi
done
