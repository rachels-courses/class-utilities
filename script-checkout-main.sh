for f in *; do
    if [ -d "$f" ]; then
    
        if [[ "$f" == *"$class"* ]]; then
          echo "--------------------------------------"
          echo "DIR: $f"
          cd $f
          echo "STASH"
          git stash
          echo "CHECKOUT MAIN"
          git checkout main
          echo "PULL"
          git pull
          cd ..
        fi
    fi
done
