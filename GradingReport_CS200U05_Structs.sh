path="/media/rachelwil/RachelsSSD1/TEACHING/STUDENT-REPOS/2024-01/CS200/u05_Struct_report.html"
rm $path
touch $path

echo "<head>" >> $path
echo "<title> u05_Structs </title>" >> $path
echo "<style type='text/css'>" >> $path
echo "pre { border: solid 1px #000; padding: 10px; }" >> $path
echo "pre.output { background: #111; color: #eee; }" >> $path
echo "tr { vertical-align:top; }" >> $path
echo "td, th { text-align: left; width: 50%; }" >> $path
echo "nav { float: left; width: 15%; position: fixed; }" >> $path
echo "nav ul li {  }" >> $path
echo "section { float: right; width: 84%; } " >> $path
echo "</style>" >> $path

echo "<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/default.min.css'>" >> $path
echo "<script src='https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js'></script>" >> $path
echo "<script>hljs.initHighlightingOnLoad();</script>" >> $path

echo "</head><body>" >> $path

echo "<nav>" >> $path
echo "<h1>u05_Struct</h1>" >> $path

echo "<ul>" >> $path
for f in *; do
    if [ -d "$f" ]; then
    
        if [[ "$f" == *"$class"* ]]; then
            echo "<li> <a href='#$f'>$f</a> </li>" >> $path
        fi
    fi
done 
echo "</ul>" >> $path
echo "</nav>" >> $path

echo "<section>" >> $path        
        
for f in *; do
    if [ -d "$f" ]; then
    
        if [[ "$f" == *"$class"* ]]; then
          echo "--------------------------------------"
          echo $f
          
          echo "<hr>" >> $path
          echo "<h2> <a href='#$f' name='$f'> $f </a> </h2>" >> $path
          
          echo "<table><tr><th> <h3>graded programs</h3> </th><th> <h3>practice programs</h3> </th></tr>" >> $path
          
          cd $f/u05_Structs/graded_program
          
          echo "<tr>" >> $path
          echo "<td>" >> $path
          echo "<h3>graded_program</h3>" >> $path
          echo "<p>Program output:</p>" >> $path
          echo "<pre class='output'>" >> $path
          
          g++ *.h *.cpp -o u05_Structs_exe
          echo "1" | ./u05_Structs_exe >> $path
          
          echo "</pre>" >> $path
          
          echo "graded_program/room_builder.cpp" >> $path
          echo "<pre><code class='cpp'>" >> $path
          cat room_builder.cpp >> $path
          echo "</code></pre>" >> $path
          
          echo "</td><td>" >> $path
          
          
          
          cd ../practice1_struct
          
          echo "practice1_struct/Pet.h" >> $path
          echo "<pre><code class='cpp'>" >> $path
          cat Pet.h >> $path
          echo "</code></pre>" >> $path
          
          echo "practice1_struct/pet_store.cpp" >> $path
          echo "<pre><code class='cpp'>" >> $path
          cat pet_store.cpp >> $path
          echo "</code></pre>" >> $path
          
          cd ../practice2_struct
          
          echo "practice2_struct/Ingredient.h" >> $path
          echo "<pre><code class='cpp'>" >> $path
          cat Ingredient.h >> $path
          echo "</code></pre>" >> $path
          
          echo "practice2_struct/recipe.cpp" >> $path
          echo "<pre><code class='cpp'>" >> $path
          cat recipe.cpp >> $path
          echo "</code></pre>" >> $path
          
          echo "</td>" >> $path
          echo "</tr></table>" >> $path
          
          cd ../../..
          pwd
        fi
    fi
done

echo "</section>" >> $path

echo "</body>" >> $path
