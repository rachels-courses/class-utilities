#!/usr/bin/python
import sys
import os
from os import listdir
from os.path import isfile, join

path = ""
separateFiles = ""
run = ""

if ( len ( sys.argv ) >= 4 ):
    run = sys.argv[3].lower()
    
if ( len ( sys.argv ) >= 3 ):
    separateFiles = sys.argv[2].lower()
    
if ( len( sys.argv ) >= 2 ):
    path = sys.argv[1]

if ( len( sys.argv ) <= 1 ):
    print( "Path required. Example: python3 build.py GRADINGPATH [separate/together] [run]" )
    exit(0)
    
    
print( "PATH:  " + path )
print( "BUILD: " + separateFiles )
print( "RUN:   " + run )

# Get the list of folders
folders = [x[0] for x in os.walk(path)]

for folder in folders:
    print( "Building files in " + folder + "..." )
       
    # Clear out the old files in this directory
    #os.system( "rm " + folder + "/*.txt" )
    os.system( "rm " + folder + "/*.info" )
    os.system( "rm " + folder + "/*.out" )
    
    # Get list of files in the directory
    # https://stackoverflow.com/questions/3207219/how-do-i-list-all-files-of-a-directory
    files = [f for f in listdir(folder) if isfile(join(folder, f))]
    
    # Build all the files as one project
    if ( separateFiles == "together" ):
        outputPath = folder + "/project.out"
        statusPath = folder + "/build.info"
        
        command = "g++ " + folder + "/*.hpp "
        command += folder + "/*.cpp"
        command += " -o " + outputPath + " > " + statusPath
        
        print( "\t" + command )
        os.system( command )
        
        # Try to run the output and put it in a file
        if ( run == "run" ):
            programOutput = folder + "/output.info"
            print( "Run " + outputPath + "..." )
            os.system( "./" + outputPath + " > " + programOutput )
        
    # Build each .cpp file separately
    else:
        for file in files:
            filepath = folder + "/" + file
            outputPath = folder + "/" + file + ".out"
            statusPath = folder + "/" + file + ".info"
            
            if ( ".cpp" in filepath ):
                command = "g++ " + filepath + " -o " + outputPath + " > " + statusPath
                print( "\t" + command )
                os.system( command )
                
                # Try to run the output and put it in a file
                if ( run == "run" ):
                    os.system( "./" + outputPath + " > " + file + "-output.info" )
                
        
        
    print( "" )
