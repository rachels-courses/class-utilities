#!/usr/bin/python
import sys
import os
from os import listdir
from os.path import isfile, join

# https://www.tutorialspoint.com/python3/python_command_line_arguments.htm
print ('Number of arguments:', len(sys.argv), 'arguments.')
print ('Argument List:', str(sys.argv))

path = ""
if ( len( sys.argv ) > 1 ):
    path = sys.argv[1]
else:
    print( "Path required. Example: python3 format.py GRADINGPATH" )
    exit(0)

print( "Path to format:" )
print( path )

# Get the list of files in this directory
# https://stackoverflow.com/questions/3207219/how-do-i-list-all-files-of-a-directory
files = [f for f in listdir(path) if isfile(join(path, f))]

print( files )
studentNames = []

for file in files:
    # Get student name
    info = file.split( "_" )
    print( "File info:" )
    print( info )
    studentName = info[0]
    lastIndex = len( info ) - 1
    filename = info[ lastIndex ]
    
    if ( studentName not in studentNames ):
        os.makedirs( path + studentName )
        studentNames.append( studentName )
        
    # Put the student's file into this directory
    os.rename( path + file, path + studentName + "/" + filename )

    print( "" )