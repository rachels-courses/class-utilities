message="U18 (operator overloading) and U19 (third party libraries) starter files"

for f in *; do
    if [ -d "$f" ]; then
    
        if [[ "$f" == *"$class"* ]]; then
          echo "--------------------------------------"
          echo "DIR: $f"
          cd $f
          echo "CHECKOUT MAIN"
          git checkout main
          echo "ADD"
          git add .
          echo "COMMIT {git commit -m \"$message\"}"
          git commit -m "$message"
          echo "PUSH"
          git push
          cd ..
        fi
    fi
done
