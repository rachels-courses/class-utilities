projectname="u03_Review_FuncClass"
origpath=$(pwd)

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color

for f in *; do
    if [ -d "$f" ]; then
    
        if [[ "$f" == *"$class"* ]]; then
          echo "--------------------------------------"
          # Find all makefiles
          find $f -name "Project_Makefile" -print0 |
            while IFS= read -r -d '' line; do
              # Does this makefile's path match our projectname?
              if [[ $line == *"$projectname"* ]]; then
                # Go here and make this file
                echo -e "${GREEN} GOTO $line ${NC}"
                cd $line
                
                echo -e "${GREEN} MAKE ${NC}"
                make > make-result.txt
                
                echo -e "${GREEN} FILES? ${NC}"
                ls                
                
                echo -e "${GREEN} BACK TO $origpath ${NC}"
                cd $origpath
              fi
            done
          #echo "DIR: $f"
          #cd $f
          #echo "STASH"
          #git stash
          #echo "PULL"
          #git pull
          #echo "BRANCH"
          #git branch
          #echo "CHECKOUT BRANCH"
          #git checkout u03_review
          #echo "BRANCH"
          #git branch
          #echo "OK?"
          ##read
          #cd ..
        fi
    fi
done
