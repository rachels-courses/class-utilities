unit_folder="u07_Strings"
path="/home/moose/STUDENT-REPOS/CS200/$unit_folder.org"
echo "PWD:"
pwd
echo "PATH: $path"
rm $path
touch $path

echo "# -*- mode:org -*-" >> $path
echo "#+TITLE: $unit_folder" >> $path

echo "* Building and running programs" >> $path

for f in *; do

    if [ -d "$f" ]; then # If this item is a directory                                                                                         

        if [[ "$f" == *"$class"* ]]; then
          echo "--------------------------------------"
          echo $f

          cd $f/$unit_folder/graded_program

          echo "** $f" >> $path
          echo "*** graded program" >> $path

          if g++ *.h *.cpp -o $unit_folder; then
              echo "*output:*" >> $path
              echo "#+BEGIN_SRC cpp :class cpp" >> $path
              ./$unit_folder test >> $path
              echo "#+END_SRC" >> $path
          else
              echo "*build failed:"* >> $path
              echo "#+BEGIN_SRC"
              g++ *.h *.cpp -o $unit_folder >> $path 2>> $path # 2nd one is error log                                                          
              echo "#+END_SRC"
          fi

          echo "*** Functions.cpp:" >> $path

          echo "#+BEGIN_SRC cpp :class cpp" >> $path
          cat Functions.cpp >> $path
          echo "#+END_SRC" >> $path

          echo "------------------------------------" >> $path

          cd ../../..
          pwd
        fi
    fi
done

echo " DIFF TIME "
echo "* AUTO DIFF" >> $path


done=()

for f1 in *; do

    # Skip if this is a file (not a directory)                                                                                                 
    if [ -f "$f1" ]; then
        continue
    fi




    for f2 in *; do

        # Skip if this is a file (not a directory)                                                                                             
        if [ -f "$f2" ]; then
            continue
        fi

        #echo "ARRAY:"                                                                                                                         
        #echo "${done[@]}"                                                                                                                     
        # Skip if we've already compared f1 vs. f2 or f2 vs. f1.                                                                               
        already_checked_this=0
        for i in "${done[@]}"; do
            # echo "* $i"                                                                                                                      
            if [[ "$i" -eq "$f2-$f1" ]]; then
                already_checked_this=1
            fi
        done

        echo "already_checked_this: $already_checked_this"

        if [[ "$already_checked_this" == "1" ]]; then
            echo "Already did $f2-$f1"
            continue
        fi
        if [[ "$f1" != "$f2" ]]; then # If these folders aren't the same                                                                       

            echo "** $f1 vs. $f2" >> $path
            wdiff -s $f1/$unit_folder/graded_program/Functions.cpp $f2/$unit_folder/graded_program/Functions.cpp >> $path

            done+="$f1-$f2"
            echo "$f1-$f2"

        fi

    done

done

echo "THE END!" >> $path






