echo "Enter branch name: "
read branch

for f in *; do
    if [ -d "$f" ]; then
    
        if [[ "$f" == *"$class"* ]]; then
          echo "--------------------------------------"
          echo "DIR: $f"
          cd $f
          echo "STASH"
          git stash
          echo "PULL"
          git pull
          echo "BRANCH"
          git branch
          echo "CHECKOUT BRANCH $branch"
          git checkout $branch --
          echo "BRANCH"
          git branch
          echo "OK?"
          #read
          cd ..
        fi
    fi
done
